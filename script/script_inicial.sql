INSERT INTO public.t_cliente (codigo, cpf, data_cadastro, nome) VALUES (nextval('t_cliente_codigo_seq'), '99999999999', '2020-08-29 19:08:17.733000', 'rafael');
INSERT INTO public.t_cliente (codigo, cpf, data_cadastro, nome) VALUES (nextval('t_cliente_codigo_seq'), '11111111111', '2020-08-29 19:19:57.461000', 'julio');
INSERT INTO public.t_cliente (codigo, cpf, data_cadastro, nome) VALUES (nextval('t_cliente_codigo_seq'), '22222222222', '2020-08-29 19:22:45.183000', 'marcos');
INSERT INTO public.t_cliente (codigo, cpf, data_cadastro, nome) VALUES (nextval('t_cliente_codigo_seq'), '88888888888', '2020-08-29 19:08:17.733000', 'antonio');

INSERT INTO public.t_usuario (codigo, data_cadastro, login, permissao, senha) VALUES (nextval('t_usuario_codigo_seq'), '2020-08-29 19:05:05.881000', 'cesar', 'CAPTACAO', '1234');
INSERT INTO public.t_usuario (codigo, data_cadastro, login, permissao, senha) VALUES (nextval('t_usuario_codigo_seq'), '2020-08-29 19:03:39.717000', 'diego', 'ANALISTA', '1234');

INSERT INTO public.t_proposta (codigo, data_cadastro, status_proposta, idcliente, idusuario) VALUES (nextval('t_proposta_codigo_seq'), '2020-08-30 14:11:51.643000', 'APROVADO', 1, 1);
INSERT INTO public.t_proposta (codigo, data_cadastro, status_proposta, idcliente, idusuario) VALUES (nextval('t_proposta_codigo_seq'), '2020-08-30 14:12:41.057000', 'PENDENTE', 3, null);
INSERT INTO public.t_proposta (codigo, data_cadastro, status_proposta, idcliente, idusuario) VALUES (nextval('t_proposta_codigo_seq'), '2020-08-30 14:12:36.842000', 'NEGADO', 2, 1);