package com.web.analiseCredito.resource;

import com.web.analiseCredito.model.Cliente;
import com.web.analiseCredito.model.Usuario;
import com.web.analiseCredito.service.ClienteService;
import com.web.analiseCredito.service.UsuarioService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Pageable;


@RestController
@RequestMapping(value="/api/cliente")
@CrossOrigin(origins="*")
public class ClienteResource {

    @Autowired
    ClienteService clienteService;

    @Autowired
    UsuarioService usuarioService;

    @GetMapping(path="listar")
    @ApiOperation(value = "Lista todos os clientes cadastrados")
    public ResponseEntity<?> listar(Pageable pageable){
        return new ResponseEntity<>(clienteService.listar(), HttpStatus.OK);
    }

    @PostMapping(path="cadastrar")
    @ApiOperation(value = "Cadastra um novo cliente")
    public ResponseEntity<?> cadastrar(@Validated @RequestBody Cliente cliente, @RequestParam(name="iduser") Long codigoUsuario){
        Usuario usuario = usuarioService.pesquisarUsuarioPorCodigo(codigoUsuario);
        if(usuario.getPermissao().equals(Usuario.Permissao.ANALISTA)) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity<>(clienteService.cadastrar(cliente), HttpStatus.OK);
    }

    @PutMapping(path="alterar")
    @ApiOperation(value = "Altera um cliente existente")
    public ResponseEntity<?> alterar(@RequestBody Cliente cliente){
        return new ResponseEntity<>(clienteService.alterar(cliente), HttpStatus.OK);
    }

    @DeleteMapping(path="remover/{id}")
    @ApiOperation(value = "Remove um cliente")
    public ResponseEntity<?> remover(@PathVariable(name="id") Long codigo) {
        clienteService.remover(codigo);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(path="codigo/{id}")
    @ApiOperation(value = "Filtro de cliente por código")
    public ResponseEntity<?> pesquisarClientePorCodigo(@PathVariable(name="id") Long codigo) {
        return new ResponseEntity<>(clienteService.pesquisarClientePorCodigo(codigo), HttpStatus.OK);
    }

    @GetMapping(path="cpf/{cpf}")
    @ApiOperation(value = "Filtro de cliente por CPF")
    public ResponseEntity<?> pesquisarClientePorCpf(@PathVariable(name="cpf") String cpf) {
        return new ResponseEntity<>(clienteService.pesquisarClientePorCpf(cpf.trim()), HttpStatus.OK);
    }

    @GetMapping(path="listar/propostas/pendentes")
    @ApiOperation(value = "Filtro de clientes com análise de proposta pendente")
    public ResponseEntity<?> listarPropostasPendentes(Pageable pageable) {
        return new ResponseEntity<>(clienteService.listarClientesComPropostasPendente(), HttpStatus.OK);
    }

}
