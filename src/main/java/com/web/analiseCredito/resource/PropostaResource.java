package com.web.analiseCredito.resource;

import com.web.analiseCredito.model.Cliente;
import com.web.analiseCredito.model.Proposta;
import com.web.analiseCredito.model.Usuario;
import com.web.analiseCredito.service.PropostaService;
import com.web.analiseCredito.service.UsuarioService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value="/api/proposta")
@CrossOrigin(origins="*")
public class PropostaResource {

    @Autowired
    PropostaService propostaService;

    @Autowired
    UsuarioService usuarioService;

    @GetMapping(path="listar")
    @ApiOperation(value = "Lista todas as propostas cadastradas")
    public ResponseEntity<?> listar(Pageable pageable, @RequestParam(name="iduser") Long codigoUsuario){
        Usuario usuario = usuarioService.pesquisarUsuarioPorCodigo(codigoUsuario);
        if(usuario.getPermissao().equals(Usuario.Permissao.CAPTACAO)) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity<>(propostaService.listar(), HttpStatus.OK);
    }

//    @GetMapping(path="listar/ativas")
//    public ResponseEntity<?> listarAtivas(Pageable pageable){
//        return new ResponseEntity<>(propostaService.listarPropostasAtivas(), HttpStatus.OK);
//    }

    @PostMapping(path="cadastrar")
    @ApiOperation(value = "Cadastra uma nova proposta")
    public ResponseEntity<?> cadastrar(@Validated @RequestBody Proposta proposta, @RequestParam(name="iduser") Long codigoUsuario){
        Usuario usuario = usuarioService.pesquisarUsuarioPorCodigo(codigoUsuario);
        if(usuario.getPermissao().equals(Usuario.Permissao.CAPTACAO)) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity<>(propostaService.cadastrar(proposta), HttpStatus.OK);
    }

    @PutMapping(path="alterar")
    @ApiOperation(value = "Altera uma proposta existente")
    public ResponseEntity<?> alterar(@RequestBody Proposta proposta, @RequestParam(name="iduser") Long codigoUsuario){
        Usuario usuario = usuarioService.pesquisarUsuarioPorCodigo(codigoUsuario);
        if(usuario.getPermissao().equals(Usuario.Permissao.ANALISTA)) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity<>(propostaService.alterar(proposta), HttpStatus.OK);
    }

    @GetMapping(path="cliente/{id}")
    @ApiOperation(value = "Filtra as propostas cadastradas para um cliente")
    public ResponseEntity<?> pesquisarPropostasDoCliente(@PathVariable(name="id") Long codigo, @RequestParam(name="iduser") Long codigoUsuario){
        Usuario usuario = usuarioService.pesquisarUsuarioPorCodigo(codigoUsuario);
        if(usuario.getPermissao().equals(Usuario.Permissao.ANALISTA)) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity<>(propostaService.carregarPropostasPorCliente(codigo), HttpStatus.OK);
    }

    @PostMapping(path="aprovar/{id}")
    @ApiOperation(value = "Aprova a proposta de um cliente")
    public ResponseEntity<?> aprovarProposta(@PathVariable(name="id") Long codigo, @RequestParam(name="iduser") Long codigoUsuario){
        Usuario usuario = usuarioService.pesquisarUsuarioPorCodigo(codigoUsuario);
        if(usuario.getPermissao().equals(Usuario.Permissao.CAPTACAO)) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity<>(propostaService.aprovarProposta(codigo, usuario), HttpStatus.OK);
    }

    @PostMapping(path="negar/{id}")
    @ApiOperation(value = "Nega a proposta de um cliente")
    public ResponseEntity<?> negarProposta(@PathVariable(name="id") Long codigo, @RequestParam(name="iduser") Long codigoUsuario){
        Usuario usuario = usuarioService.pesquisarUsuarioPorCodigo(codigoUsuario);
        if(usuario.getPermissao().equals(Usuario.Permissao.CAPTACAO)) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity<>(propostaService.negarProposta(codigo, usuario), HttpStatus.OK);
    }

    @DeleteMapping(path="remover/{id}")
    @ApiOperation(value = "Remove uma proposta cadastrada")
    public ResponseEntity<?> remover(@PathVariable(name="id") Long codigo) {
        propostaService.remover(codigo);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
