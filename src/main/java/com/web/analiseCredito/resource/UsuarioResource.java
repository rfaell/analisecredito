package com.web.analiseCredito.resource;

import com.web.analiseCredito.model.Cliente;
import com.web.analiseCredito.model.Usuario;
import com.web.analiseCredito.service.PropostaService;
import com.web.analiseCredito.service.UsuarioService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value="/api/usuario")
@CrossOrigin(origins="*")
public class UsuarioResource {

    @Autowired
    UsuarioService usuarioService;

    @GetMapping(path="listar")
    @ApiOperation(value = "Lista todos os usuários cadastrados")
    public ResponseEntity<?> listar(Pageable pageable){
        return new ResponseEntity<>(usuarioService.listar(), HttpStatus.OK);
    }

    @PostMapping(path="cadastrar")
    @ApiOperation(value = "Cadastra um novo usuário")
    public ResponseEntity<?> cadastrar(@Validated @RequestBody Usuario usuario){
        return new ResponseEntity<>(usuarioService.cadastrar(usuario), HttpStatus.OK);
    }

    @PutMapping(path="alterar")
    @ApiOperation(value = "Altera um usuário existentes")
    public ResponseEntity<?> alterar(@RequestBody Usuario usuario){
        return new ResponseEntity<>(usuarioService.alterar(usuario), HttpStatus.OK);
    }

    @DeleteMapping(path="remover/{id}")
    @ApiOperation(value = "Remove um usuário")
    public ResponseEntity<?> remover(@PathVariable(name="id") Long codigo) {
        usuarioService.remover(codigo);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
