package com.web.analiseCredito;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnaliseCreditoApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnaliseCreditoApplication.class, args);
		System.out.println("\n\nLink para o swagger: http://localhost:8080/swagger-ui.html#!/");
	}


}
