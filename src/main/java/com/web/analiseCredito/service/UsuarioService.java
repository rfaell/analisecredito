package com.web.analiseCredito.service;

import com.web.analiseCredito.erro.ResourceNotFoundException;
import com.web.analiseCredito.model.Cliente;
import com.web.analiseCredito.model.Usuario;
import com.web.analiseCredito.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class UsuarioService {

    @Autowired
    UsuarioRepository usuarioRepository;

    public List<Usuario> listar() {
        return usuarioRepository.findAll();
    }

    public void remover(Long codigo) {
        Usuario usuario = pesquisarUsuarioPorCodigo(codigo);
        if(usuario == null) {
            throw new ResourceNotFoundException("Usuario não encontrado");
        }
        usuarioRepository.delete(usuario);
    }

    public Usuario alterar(Usuario usuario) {
        Usuario usuarioSaved = pesquisarUsuarioPorCodigo(usuario.getCodigo());
        if(usuarioSaved == null) {
            throw new ResourceNotFoundException("Usuario inexistente");
        }
        return usuarioRepository.save(usuario);
    }

    public Usuario cadastrar(Usuario usuario) {
        usuario.setDataCadastro(new Date());
        return usuarioRepository.save(usuario);
    }

    public Usuario pesquisarUsuarioPorCodigo(Long codigo) {
        if(codigo != null && codigo > 0) {
            Usuario usuario = usuarioRepository.findByCodigo(codigo);
            if(usuario == null) {
                throw new ResourceNotFoundException("Usuario não encontrado");
            }
            return usuario;
        }
        throw new ResourceNotFoundException("Por favor preencha o codigo do usuario");
    }

}
