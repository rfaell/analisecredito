package com.web.analiseCredito.service;

import com.web.analiseCredito.erro.ResourceNotFoundException;
import com.web.analiseCredito.model.Cliente;
import com.web.analiseCredito.model.Proposta;
import com.web.analiseCredito.model.Usuario;
import com.web.analiseCredito.repository.ClienteRepository;
import com.web.analiseCredito.repository.PropostaRepository;
import com.web.analiseCredito.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class PropostaService {

    @Autowired
    PropostaRepository propostaRepository;

    @Autowired
    ClienteRepository clienteRepository;

    @Autowired
    UsuarioRepository usuarioRepository;

    public Proposta cadastrar(Proposta proposta) {
        proposta.setStatusProposta(Proposta.Status.PENDENTE);
        proposta.setDataCadastro(new Date());
        return propostaRepository.save(proposta);
    }

    public List<Proposta> listar() {
        return propostaRepository.findAll();
    }

//    public List<Proposta> listarPropostasAtivas() {
//        return propostaRepository.findPropostasAtiva();
//    }

//SOFT DELETE
//    public void remover(Long codigo){
//        Proposta proposta = carregarProposta(codigo);
//        proposta.setStatusProposta(Proposta.Status.CANCELADO);
//        propostaRepository.save(proposta);
//    }

    public void remover(Long codigo) {
        Proposta proposta = carregarProposta(codigo);
        if(proposta == null) {
            throw new ResourceNotFoundException("Proposta não encontrado");
        }
        propostaRepository.delete(proposta);
    }

    public List<Proposta> carregarPropostasPorCliente(Long codigo) {
        Cliente cliente = clienteRepository.findByCodigo(codigo);
        if(cliente == null) {
            throw new ResourceNotFoundException("Cliente não encontrado");
        }
        List<Proposta> resultado = propostaRepository.findByClienteCodigo(codigo);
        return resultado;
    }

    public Proposta alterar(Proposta proposta) {
        carregarProposta(proposta.getCodigo());
        return propostaRepository.save(proposta);
    }

    public Proposta carregarProposta(Long codigo) {
        Proposta proposta = propostaRepository.findByCodigo(codigo);
        if(proposta == null) {
            throw new ResourceNotFoundException("Proposta não encontrada");
        }
        return proposta;
    }

    public Proposta aprovarProposta(Long codigo, Usuario usuario) {
        Proposta proposta = carregarProposta(codigo);
        proposta.setStatusProposta(Proposta.Status.APROVADO);
        proposta.setUsuarioAprovacao(usuario);
        return propostaRepository.save(proposta);
    }

    public Proposta negarProposta(Long codigo, Usuario usuario) {
        Proposta proposta = carregarProposta(codigo);
        proposta.setStatusProposta(Proposta.Status.NEGADO);
        proposta.setUsuarioAprovacao(usuario);
        return propostaRepository.save(proposta);
    }

}
