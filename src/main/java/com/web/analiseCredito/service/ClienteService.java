package com.web.analiseCredito.service;

import com.web.analiseCredito.erro.ResourceNotFoundException;
import com.web.analiseCredito.model.Cliente;
import com.web.analiseCredito.model.Proposta;
import com.web.analiseCredito.repository.ClienteRepository;
import com.web.analiseCredito.repository.PropostaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class ClienteService {

    @Autowired
    ClienteRepository clienteRepository;

    @Autowired
    PropostaRepository propostaRepository;

    public List<Cliente> listar() {
        return  clienteRepository.findAll();
    }

    public Cliente cadastrar(Cliente cliente) {
        Cliente clienteSaved = pesquisarClientePorCpf(cliente.getCpf());
        if(clienteSaved != null) {
            throw new ResourceNotFoundException("CPF já cadastrado para o cliente de codigo " + clienteSaved.getCodigo());
        }
        cliente.setDataCadastro(new Date());
        return clienteRepository.save(cliente);
    }

    public Cliente alterar(Cliente cliente) {
        Cliente clienteSaved = pesquisarClientePorCodigo(cliente.getCodigo());
            if(clienteSaved == null) {
                throw new ResourceNotFoundException("Cliente inexistente");
            }
            return clienteRepository.save(cliente);
    }

    public Cliente pesquisarClientePorCodigo(Long codigo) {
        if(codigo != null && codigo > 0) {
            Cliente cliente = clienteRepository.findByCodigo(codigo);
            if(cliente == null) {
                throw new ResourceNotFoundException("Cliente não encontrado");
            }
            return cliente;
        }
        throw new ResourceNotFoundException("Por favor preencha o codigo do cliente");
    }

    public void remover(Long codigo) {
        if(!existePropostaParaCliente(codigo)) {
            Cliente cliente = pesquisarClientePorCodigo(codigo);
            if(cliente == null) {
                throw new ResourceNotFoundException("Cliente não encontrado");
            }
            clienteRepository.delete(cliente);
        }
        throw new ResourceNotFoundException("Este cliente não pode ser removido pois existe uma ou mais propostas cadastrada para ele");
    }

    public boolean existePropostaParaCliente(Long codigo) {
        List<Proposta> resultado = propostaRepository.findByClienteCodigo(codigo);
        if(resultado != null && !resultado.isEmpty()) {
            return true;
        }
        return false;
    }

    public Cliente pesquisarClientePorCpf(String cpf) {
        if (cpf == null || cpf.equals("")) {
            throw new ResourceNotFoundException("CPF não preenchido");
        }
        return clienteRepository.findByCpf(cpf);
    }

    public List<Cliente> listarClientesComPropostasPendente() {
        return propostaRepository.listarClientesComPropostasPendente();
    }

}
