package com.web.analiseCredito.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="T_PROPOSTA")
public class Proposta {

    private Long codigo;
    private Date dataCadastro;
    private Status statusProposta;
    private Cliente cliente;
    private Usuario usuarioAprovacao;

    //ENUM QUE DEFINE OS STATUS POSSIVEIS DE UMA PROPOSTA
    public enum Status {
        PENDENTE("P"),
        APROVADO("A"),
        NEGADO("N");

        private final String descricao;

        Status(String descricao) {
            this.descricao = descricao;
        }

        public String getDescricao() {
            return descricao;
        }

        public static Status valueOfOrdinal(int value) {
            for(Status s : values()) {
                if(s.ordinal() == value) {
                    return s;
                }
            }
            return null;
        }

        public String getName() {
            return name();
        }
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    @Enumerated(EnumType.STRING)
    public Status getStatusProposta() {
        return statusProposta;
    }

    public void setStatusProposta(Status statusProposta) {
        this.statusProposta = statusProposta;
    }

    @ManyToOne
    @JoinColumn(name="IDCLIENTE")
    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @ManyToOne
    @JoinColumn(name="IDUSUARIO")
    public Usuario getUsuarioAprovacao() {
        return usuarioAprovacao;
    }

    public void setUsuarioAprovacao(Usuario usuarioAprovacao) {
        this.usuarioAprovacao = usuarioAprovacao;
    }
}
