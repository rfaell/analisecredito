package com.web.analiseCredito.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="T_USUARIO")
public class Usuario {

    private Long codigo;
    private String login;
    private String senha;
    private Permissao permissao;
    private Date dataCadastro;

    public enum Permissao {
        ANALISTA("P"),
        CAPTACAO("C");

        private final String descricao;

        Permissao(String descricao) {
            this.descricao = descricao;
        }

        public String getDescricao() {
            return descricao;
        }

        public static Permissao valueOfOrdinal(int value) {
            for(Permissao s : values()) {
                if(s.ordinal() == value) {
                    return s;
                }
            }
            return null;
        }

        public String getName() {
            return name();
        }
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    @Enumerated(EnumType.STRING)
    public Permissao getPermissao() {
        return permissao;
    }

    public void setPermissao(Permissao permissao) {
        this.permissao = permissao;
    }
}
