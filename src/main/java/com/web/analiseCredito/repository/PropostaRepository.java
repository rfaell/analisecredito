package com.web.analiseCredito.repository;

import com.web.analiseCredito.model.Cliente;
import com.web.analiseCredito.model.Proposta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PropostaRepository extends JpaRepository<Proposta, Integer> {

    @Query("SELECT p FROM Proposta p WHERE p.cliente.codigo = ?1")
    List<Proposta> findByClienteCodigo(Long codigo);

    Proposta findByCodigo(Long codigo);

//    @Query("SELECT p FROM Proposta p WHERE p.statusProposta <> 'CANCELADO'")
//    List<Proposta> findPropostasAtiva();

    @Query("SELECT  DISTINCT(p.cliente) FROM Proposta p WHERE p.statusProposta = 'PENDENTE'")
    List<Cliente> listarClientesComPropostasPendente();

}
