package com.web.analiseCredito.repository;

import com.web.analiseCredito.model.Cliente;
import com.web.analiseCredito.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {

    Usuario findByCodigo(Long codigo);
}
