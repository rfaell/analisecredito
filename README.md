# Analise de Credito

Foi criada uma aplicação com o back-end em Spring Boot. Para persistir os dados foi utilizado banco de dados PostgreSQL. Como gerenciador de dependencias foi utilizado o Maven. Foi criada uma pequena interface em Swagger para consumir as APIs.


# Executando o projeto

1.  Executar o script de criação de banco localizado  na pasta script -> script_bd.sql 
2.  Rodar a aplicação AnaliseCreditoApplication.java ou executar o arquivo analiseCredito-0.0.1-SNAPSHOT.java na pasta target do projeto.
3. Executar o script para popular a base de dados localizado na pasta script -> script_inicial.sql
4.  Abrir no navegador  [http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html) para acessar a aplicação


# Dicionário

Cliente - Possíveis portadores de cartões 

Usuário - Usuário do sistema podendo ser Analista e Captação

Proposta - Conjunto de informações para a análise de crédito de um possível portador            

# Executando as operações no Swagger

### Operação "Filtro de clientes com análise de proposta pendente"
Este serviço pode ser executado por qualquer usuário e exibe todos os clientes com propostas pendentes

### Operação "Cadastrar Cliente"
Este serviço só poderá ser executado por um usuário com permissão CAPTAÇÃO e cadastra possíveis portadores

### Operação "Filtra as propostas cadastradas para um cliente"
Este serviço só poderá ser executado por um usuário com permissão CAPTAÇÃO e filtra as propostas cadastradas para um cliente especifico

### Operação "Filtra as propostas cadastradas para um cliente"
Este serviço só poderá ser executado por um usuário com permissão CAPTAÇÃO e filtra as propostas cadastradas para um cliente especifico  

### Operação "Cadastrar uma nova Proposta"
Este serviço só poderá ser executado por um usuário com permissão ANALISTA e cadastra uma proposta para um cliente

### Operação "Aprova a proposta de um cliente"
Este serviço só poderá ser executado por um usuário com permissão ANALISTA e aprova uma proposta cadastrada

### Operação "Nega a proposta de um cliente"
Este serviço só poderá ser executado por um usuário com permissão ANALISTA e nega uma proposta cadastrada

### Operação "Lista todas as propostas cadastradas"
Este serviço só poderá ser executado por um usuário com permissão ANALISTA e exibe a lista de propostas cadastradas no sistema

### Outras operações
As demais operações são para manter e visualizar as entidades do sistema
